(ns factorial.core
    (:gen-class))

(defn -main []
    (defn factorial [n]
        (if (= n 0) 1
            (* n (factorial (dec n)))))
    (factorial 3))
